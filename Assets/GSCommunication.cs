﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;
using UnityEngine.UI;

public class GSCommunication : MonoBehaviour 
{
	[Header("Connection")]	
	public string username;
	public string password;
	[Header("isConnected")]
	public string ConnectedUser;
	public string LastState;
	[Header("Data to pass")]
	public int score;
	[Header("Links to GO")]
	public Text PlayerDetails;


	void Awake()
	{
		Debug.Log("CREATE DELEGATES");
		GameSparks.Api.Messages.NewHighScoreMessage.Listener += HighScoreHandleMessage;
		GameSparks.Api.Messages.AchievementEarnedMessage.Listener += AchievementHandleMessage;
	}

	void OnDestroy()
	{
		Debug.Log ("DESTROY");
		GameSparks.Api.Messages.NewHighScoreMessage.Listener -= HighScoreHandleMessage;
		GameSparks.Api.Messages.AchievementEarnedMessage.Listener -= AchievementHandleMessage;
	}



	public void BuyGood()
	{
		new GameSparks.Api.Requests.BuyVirtualGoodsRequest()
			.SetCurrencyType(1)
			.SetQuantity(1)
			.SetShortCode("NUTS")
			.Send ((response) => {
				if(!response.HasErrors)
				{
					Debug.Log("Good Bought ...");
					UpdatePlayerDetails();
				}
				else
				{
					Debug.LogError("Good not bought ...");
				}
			});
	}


	public void GrantCurrency()
	{
		new GameSparks.Api.Requests.LogEventRequest ()
			.SetEventKey ("GRANT_CUR")
			.SetEventAttribute ("CASH", 1)
			.Send ((response) => {
				if(!response.HasErrors)
				{
					Debug.Log("Currency +1");
					UpdatePlayerDetails();
				}
				else
				{
					Debug.LogError("Currency grant error ...");
				}
					
		});
	}



	public void UpdatePlayerDetails()
	{
		new GameSparks.Api.Requests.AccountDetailsRequest ().Send ((response) => {
			if(!response.HasErrors)
			{
				Debug.Log("Player Data received ...");
				PlayerDetails.text = response.DisplayName + "\n" +
					" Actual Currency " + response.Currency1 + "\n" +
					" Actual Goods " + response.VirtualGoods.GetNumber("NUTS");
			}	
			else
			{
				Debug.LogError("Player Data not received...");
			}
		});
	}




	public void Authentify()
	{
		Authentification (username,password);
	}

	public void Award()
	{
		new GameSparks.Api.Requests.LogEventRequest ()
			.SetEventKey ("AWARD_ACHVT")
			.Send ((response) => {
				if(!response.HasErrors)
				{
					Debug.Log("Achievement requested ...");
				}
				else
				{
					Debug.LogError("Achievement request error...");
				}
		});
	}



	void HighScoreHandleMessage(GameSparks.Api.Messages.NewHighScoreMessage _message)
	{
		Debug.Log ("New High Score : "  +  _message.LeaderboardData.UserName + " " + _message.LeaderboardData.JSONData["SCORE"]);
	}


	void AchievementHandleMessage(GameSparks.Api.Messages.AchievementEarnedMessage _message)
	{
		Debug.Log ("Achieved : " + _message.AchievementName);
	}



	void PullScore()
	{
		new GameSparks.Api.Requests.LeaderboardDataRequest ()
			.SetLeaderboardShortCode ("HIGHSCORE_LEADERBOARD")
			.SetEntryCount (100)
			.Send ((response) => {
				if(!response.HasErrors)
				{
					Debug.Log("Leaderboard data found...");
					foreach(GameSparks.Api.Responses.LeaderboardDataResponse._LeaderboardData entry in response.Data)
					{
						Debug.Log("Rank " + (int) entry.Rank + "   " + entry.UserName + "   " + entry.JSONData["SCORE"].ToString()+"\n--------------------");

					}
				}	
				else
				{
					Debug.LogError("Leaderboard data error...");
				}
		});
	}



	public void InsertScore()
	{
		new GameSparks.Api.Requests.LogEventRequest ().SetEventKey ("SUBMIT_SCORE")
			.SetEventAttribute("SCORE",score)
			.Send ((response) => {
			if(!response.HasErrors)
			{
				Debug.Log("Score Submitted");
			}	
			else
			{
				Debug.LogError("Score Submittion error...");
			}
		});
	}



	void LoadScore()
	{
		new GameSparks.Api.Requests.LogEventRequest().SetEventKey("LOAD_PLAYER").Send((response) => 
		{
			if(!response.HasErrors)
			{
				Debug.Log("Received Player Data from GS");
				GSData data = response.ScriptData.GetGSData("player_Data") ;
					score = (int) data.GetInt("overallNuts");
			}
			else
			{
				Debug.LogError("Extraction error for scores");
			}
		});
	}




	void SaveScore(int score)
	{
		new GameSparks.Api.Requests.LogEventRequest()
			.SetEventKey("SAVE_PLAYER")
			.SetEventAttribute("OVERALLNUTS", score)
			.Send((response) =>
				{
					if (!response.HasErrors) 
					{
						Debug.Log("Score Saved …");
						LastState = "Saved";
					}
					else
					{
						Debug.Log("Error Saving Score");
						LastState = "Error";
					}	
				});

	}




	void Authentification(string name, string password)
	{
		new GameSparks.Api.Requests.AuthenticationRequest()
			.SetPassword(password)
			.SetUserName(name)
			.Send((response) =>
				{
					if (!response.HasErrors) 
					{
						Debug.Log("Player Authetified …");
						ConnectedUser = response.DisplayName;
						LastState = "Connected";
						UpdatePlayerDetails();
					}
					else
					{
						Debug.Log("Error Authentifing Player");
						LastState = "Error";
					}	
				});
	}




	void RegisterPlayer(string displayname, string name, string password)
	{
	new GameSparks.Api.Requests.RegistrationRequest()
			.SetDisplayName(displayname)
			.SetPassword(password)
			.SetUserName(name)
			.Send((response) => 
				{
					if (!response.HasErrors) 
						{
							Debug.Log("Player Registered …");
						}
						else
						{
							Debug.Log("Error Registering Player");
						}
				});
	}
}
