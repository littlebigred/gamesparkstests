﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Api;
using GameSparks.Core;

public class GSInsertData : MonoBehaviour 
{

	public string field;
	public string value;


	public void InsertInNoSQL()
	{
		new GameSparks.Api.Requests.LogEventRequest()
			.SetEventKey("INSERT_POK")
			.SetEventAttribute(field,value)
			.Send((response) => {
				if(!response.HasErrors)
				{
					Debug.Log("Insert as been made ...");
				}
				else
				{
					Debug.LogError("Insert error (" + field + "/" + value +")");
				}
			});
	}




}
